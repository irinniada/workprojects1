﻿using System;


namespace Cursova
{
    class Configuration
    {
         private static Configuration oCFG;

         private Configuration() { }

         public static Configuration getInstance()
        {
            if (oCFG == null)
            {
                oCFG = new Configuration();
            }

            return oCFG;
        }

       
        private const int iWidth = 396, iHeight = 640;

        private static long lTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;


        public int getWidth()
        {
            return iWidth;
        }

        public int getHeight()
        {
            return iHeight;
        }

        public long getCurrentTime()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public long getTime()
        {
            return lTime;
        }

        public void setTime(long lTime)
        {
            Configuration.lTime = lTime;
        }
    }
}
